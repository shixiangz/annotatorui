// var server = "http://192.168.56.101:8000";
var server = "http://localhost:8000";
var jsonStr = null;

$(document).ready(function() {
	initTabs();
	$.ajaxSetup ({
		cache: false
	});
	// getDictList();
});

function initTabs() {
	$("#content").find("[id^='tab']").hide();
	$("#tabs li:first").attr("id","current");
	$("#content #tab1").fadeIn();
	
	$('#tabs a').click(function(e) {
		e.preventDefault();
		if ($(this).closest("li").attr("id") == "current") {
			return;
		} else {
			$("#content").find("[id^='tab']").hide();
			$("#tabs li").attr("id","");
			$(this).parent().attr("id","current");
			$('#' + $(this).attr('name')).fadeIn();
		}
	});
}

function insertSampleText() {
	sampleText = "Novel experiments with Ultrasound Associated with High Frequency Electromagnetic Field (UAHFEMF) irradiation on rats and mice found evidences of characteristic Alzheimer's disease (AD) degenerations including neurite plaques, beta-amyloid, TAU plaque and deposition in cells. Concomitant passive avoidance test was performed on six mice, and all showed signs of visual and auditory agnosia and lost cognition of threatening condition. The post section Thioflavin-S fluorescent microscopy found dilated ventricles and dense amyloid-deposition in Ca3 and dentate gyrus. A human urothelial cell activation RhoGTPase-activating protein (TAGAP) isoform b homolog (GenBank accession # P84107) induced in the UAHFEMF-treated rat brain was identified using electron spray ionization (ESI) liquid chromatography tandem mass spectrometry (LC/MS/MS). We hypothesized that one of the causes of ABC can be the UAHFEMF discharges in human brain."
	$("#text").val(sampleText);
	// $("#text").val('Cancer-selective targeting of the NF-XB survival pathway with GADD45A/MKK7 inhibitors.');
	return false;
}

function clearText() {
	$("#text").val('');
}

function getAnnotationBtnClicked() {
	if (!integrityCheck()) {
		return;
	}
	var self = this;
	var data = getParameters();
	var url = server + "/getAnnotation";
	$.ajax({
		url: url,
		type: "GET",
		timeout: 5000,
		data: data,
		dataType: 'html',
		success: function(html) {
			renderAnnotation(html);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			if (textStatus === "timeout") {
				return;
			}
			return;
		}
	});
}

function integrityCheck() {
	text = $('#text').val();
	dictionaries = $('.selectpicker').val();
	if (text === '') {
		alert('Please enter text!');
		return false;
	}
	if (dictionaries === null) {
		alert('Please select dictionaries!');
		return false;
	}
	return true;
}

function getParameters() {
	var data = {};
	data['text'] = $('#text').val();
	data['dictionaries'] = $('.selectpicker').val();
	data['findAllMatched'] = $('input[name="radio_find_all_matches"]:checked').val();
	data['caseSensitive'] = $('input[name="radio_case_sensitive"]:checked').val();
	data['orderIndependentLookup'] = $('input[name="radio_order_independent_lookup"]:checked').val();
	data['searchStrategy'] = $('#search_strategy').val();

	return data;
}

function renderAnnotation(html) {
	document.open();
	document.write(html);
	document.close();
}

function getDictList() {
	var self = this;
	var url = server + "/dictionaries";
	$.ajax({
		url: url,
		type: "GET",
		success: function(json) {
			dictList = json['dictList'];
			renderDictList(dictList);
		},
		fail: function(json) {

		}
	});
}

function renderDictList(dictList) {
	var html = '';
	for (key in dictList) {
		var dict = dictList[key];
		var option = "<option>" + dict + "</option>";
		html += option;
	}
	$('.selectpicker').html(html);
}

function getJson() {
	var self = this;
	var url = server + "/getJson";
	$.ajax({
		url: url,
		type: "GET",
		success: function(json) {
			if (json['text'] === 'undefined') {
				$('#tab_wrapper').hide();
				return;
			}
			jsonStr = JSON.stringify(json);
			$("#tab2").html(jsonStr);
			renderTable(json);
			finishQuery();
		},
		fail: function(json) {

		}
	});
}

function deleteJsonFile() {
	var self = this;
	var url = server + "/deleteJsonFile";
	$.ajax({
		url: url,
		type: "POST",
		success: function(json) {

		},
		fail: function(json) {

		}
	});
}

function finishQuery() {
	var self = this;
	var url = server + "/finishQuery";
	$.ajax({
		url: url,
		type: "POST",
		success: function(json) {

		},
		fail: function(json) {

		}
	});
}

function renderTable(json) {
	var data = {
		column1: "Term",
		column2: "Span",
		column3: "Object"
	};
	var html = $("#table_template").render(data);
	$("#tab3").append(html);

	var text = json['text'];
	var denotations = json['denotations'];
	for (index in denotations) {
		var denotation = denotations[index];
		var span = denotation['span'];
		var term = text.substring(span['begin'], span['end']);
		var object = denotation['obj'];
		var tableRowData = {
			term: term,
			span: span['begin'] + '-' + span['end'],
			object: object
		}
		$("#output_table tbody").append($("#table_row_template").render(tableRowData));
	}

	$("#output_table").DataTable();
}

function uploadFile() {
	var form_data = new FormData($('#upload-file')[0]);
	var url = server + '/uploadajax';
	$.ajax({
		type: 'POST',
		url: url,
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		async: false,
		success: function(data) {
			console.log('Success!');
		},
	});
}
